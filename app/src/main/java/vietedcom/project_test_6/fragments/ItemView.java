package vietedcom.project_test_6.fragments;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import vietedcom.project_test_6.Model;
import vietedcom.project_test_6.R;


/**
 * Created by Smile on 3/31/2016.
 */
public class ItemView extends RelativeLayout {
    private ImageView mAvatar;
    private TextView mName, mProject, mRole;

    public ItemView(Context context) {
        super(context);
        init();
    }


    public ItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ItemView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.item_list, this);
        mAvatar = (ImageView) findViewById(R.id.avatar);
        mName = (TextView) findViewById(R.id.name);
        mProject = (TextView) findViewById(R.id.project);
        mRole = (TextView) findViewById(R.id.gender);
    }

    public void display(Model model) {
        String name, projectName, role;
        int avatar;

        if (model == null) {
            name = "";
            projectName = "";
            role = "";
            avatar = R.drawable.female;
        } else {
            name = model.getName();
            if (name == null) {
                name = "";
            }
            Model.Project project = model.getProject();
            if (project == null) {
                projectName = "";
                role = "";
            } else {
                projectName = project.getName();
                if (projectName == null) {
                    projectName = "";
                }

                role = project.getRole();
                if (role == null) {
                    role = "";
                }
            }

            switch (model.getGender()) {
                case Model.MALE:
                    avatar = R.drawable.male;
                    break;
                case Model.FEMALE:
                    avatar = R.drawable.female;
                    break;
                default:
                    avatar = R.drawable.female;
                    break;
            }

        }
        mName.setText(name);
        mProject.setText(projectName);
        mRole.setText(role);
        mAvatar.setImageResource(avatar);
    }
}
