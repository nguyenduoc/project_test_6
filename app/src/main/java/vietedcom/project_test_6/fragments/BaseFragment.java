package vietedcom.project_test_6.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import vietedcom.project_test_6.MainActivity;

/**
 * Created by Smile on 5/30/2016.
 */
public class BaseFragment extends Fragment {
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            MainActivity mainActivity = (MainActivity) activity;
            if (this instanceof HomeFragment||this instanceof AboutFragment) {
                mainActivity.hideMenu();
            } else if (this instanceof ListPersonFragment) {
                mainActivity.showMenu();
            }
        }
    }
}
