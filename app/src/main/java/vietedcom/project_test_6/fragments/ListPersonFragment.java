package vietedcom.project_test_6.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import vietedcom.project_test_6.Model;
import vietedcom.project_test_6.R;
import vietedcom.project_test_6.apis.DataRequest;
import vietedcom.project_test_6.apis.OnDataChangedListener;
import vietedcom.project_test_6.apis.OnErrorListener;


/**
 * Created by Smile on 3/31/2016.
 */
public class ListPersonFragment extends BaseFragment implements OnDataChangedListener<ArrayList<Model>>, OnErrorListener {

    private static final String SAVE_MODEL = "save_model";
    private static final String IS_GET_DATA = "is_get_data";
    private ArrayList<Model> mModels;
    private DataRequest mDataRequest;
    private boolean isGetData = true;

    private RecyclerView mRecyclerView;
    private View mLoading, mNoResultView, mErrorView, mRetryView;
    private TextView mMessageView;

    private ListAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null)
            restoreData(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycle_view);
        mLoading = view.findViewById(R.id.view_loading);
        mNoResultView = view.findViewById(R.id.no_result);
        mMessageView = (TextView) view.findViewById(R.id.message);
        mErrorView = view.findViewById(R.id.error_view);
        view.findViewById(R.id.retry).setOnClickListener(mRetryListener);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new ListAdapter();
        mRecyclerView.setAdapter(mAdapter);
    }

    private View.OnClickListener mRetryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            requestData();
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        Log.e("ND", ">>>>> activity create");
        super.onActivityCreated(savedInstanceState);
        if (mModels != null && mModels.size() > 0 && !isGetData)
            refreshData();
        else
            requestData();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(SAVE_MODEL, mModels);
        if (mDataRequest != null && mDataRequest.isRequesting())
            outState.putBoolean(IS_GET_DATA, mDataRequest.isRequesting());
    }

    @Override
    public void onDestroy() {
        if (mDataRequest != null) {
            mDataRequest.cancelRequest();
        }
        super.onDestroy();
    }

    private void refreshData() {
        isGetData = false;
        if (mModels != null && mModels.size() > 0) {
            mAdapter.setDataSource(mModels);
            showViewData();
        } else {
            if (mDataRequest == null || !mDataRequest.isError()) {
                showViewNoResult();
            }
        }
    }

    private void restoreData(Bundle bundle) {
        mModels = bundle.getParcelable(SAVE_MODEL);
        isGetData = bundle.getBoolean(IS_GET_DATA);
    }

    private void requestData() {
        showViewLoading();
        if (mDataRequest != null)
            mDataRequest.cancelRequest();
        mDataRequest = new DataRequest();
        mDataRequest.setOnDataChangedListener(this);
        mDataRequest.setOnErrorListener(this);
        mDataRequest.startRequest();
    }

    @Override
    public void dataChanged(ArrayList<Model> model) {
        mModels = model;
        refreshData();
    }

    @Override
    public void error(String message) {
        showViewError(message);
    }

    private void showViewLoading() {
        mLoading.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.GONE);
        mNoResultView.setVisibility(View.GONE);
        mErrorView.setVisibility(View.GONE);
    }

    private void showViewData() {
        mLoading.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mNoResultView.setVisibility(View.GONE);
        mErrorView.setVisibility(View.GONE);
    }

    private void showViewNoResult() {
        mLoading.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mNoResultView.setVisibility(View.VISIBLE);
        mErrorView.setVisibility(View.GONE);
    }

    private void showViewError(String mesage) {
        if (mesage == null) {
            mesage = "";
        }
        mMessageView.setText(mesage);

        mLoading.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);
        mNoResultView.setVisibility(View.GONE);
        mErrorView.setVisibility(View.VISIBLE);

    }

    public void reloadDataClick() {
        requestData();
    }
}
