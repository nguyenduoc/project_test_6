package vietedcom.project_test_6.fragments;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;

import vietedcom.project_test_6.Model;

/**
 * Created by Smile on 3/31/2016.
 */
public class ListAdapter extends RecyclerView.Adapter {
    private ArrayList<Model> mItems;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (parent == null) return null;
        return new AppViewHolder(new ItemView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((ItemView) holder.itemView).display(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        if (mItems != null) return mItems.size();
        return 0;
    }

    public void add(ArrayList<Model> models) {
        if (models == null || models.size() == 0) return;

        if (mItems == null) {
            mItems = models;
            notifyDataSetChanged();
            return;
        }
        int pos = mItems.size() - 1;
        mItems.addAll(models);
        notifyItemMoved(pos, mItems.size() - 1);

    }

    public void setDataSource(ArrayList<Model> items) {
        mItems = items;
        notifyDataSetChanged();
    }
}
