package vietedcom.project_test_6.apis;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import vietedcom.project_test_6.Model;


/**
 * Created by Smile on 3/6/2016.
 */
public class DataRequest extends AsyncTask<Void, Void, ArrayList<Model>> {
    //link object wrong
    public static final String URL_API0 = "https://api.myjson.com/bins/53x82";

    //link error
    public static final String URL_API1 = "https://api.myjson.com/bins/53x82smile";

    // link empty
    public static final String URL_API2 = "https://api.myjson.com/bins/4wafk";
    public static final String URL_API3 = "https://api.myjson.com/bins/577y8";

    private OnDataChangedListener<ArrayList<Model>> mDataChangedListener;
    private OnErrorListener mOnErrorListener;
    private Handler mHandler;
    private boolean requesting = false;
    private boolean isError = false;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        detroy(mHandler);
        mHandler = new Handler();
        requesting = true;
        isError = false;
    }

    @Override
    protected ArrayList<Model> doInBackground(Void... params) {
        String json = makeHttp();
        ParseJsonToModel parseJsonToModel = new ParseJsonToModel();
        try {
            return parseJsonToModel.parse(json);
        } catch (JSONException e) {
            pushError(e.getMessage());
        }
        return null;
    }

    public boolean isError() {
        return isError;
    }

    private void pushError(final String msg) {
        isError = true;
        if (mHandler == null) return;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                if (mOnErrorListener != null)
                    mOnErrorListener.error(msg);
            }
        });

    }


    @Override
    protected void onPostExecute(ArrayList<Model> model) {
        super.onPostExecute(model);
        requesting = false;
        if (mDataChangedListener != null)
            mDataChangedListener.dataChanged(model);

    }

    public void startRequest() {
        execute();
    }

    public boolean isRequesting() {
        return requesting;
    }

    public void cancelRequest() {
        detroy(mHandler);
        cancel(true);

    }

    public void setOnDataChangedListener(OnDataChangedListener<ArrayList<Model>> listener) {
        this.mDataChangedListener = listener;
    }

    public void setOnErrorListener(OnErrorListener listener) {
        this.mOnErrorListener = listener;
    }

    private String makeHttp() {
        Log.v("ND", "Run loading data to internet");
        URL url;
        HttpURLConnection connection = null;
        String json = null;
        try {
            url = new URL(dummyRequest());
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setConnectTimeout(20000);
            connection.setReadTimeout(10000);
            connection.setDoInput(true);
            InputStream in = new BufferedInputStream(connection
                    .getInputStream());
            json = getStringFromInputStream(in);
        } catch (IOException e) {
            pushError(e.getMessage());
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
        return json;
    }

    private String dummyRequest() throws IOException {
        Random random = new Random();
        switch (random.nextInt(5)) {
            case 0:
                return URL_API0;

            case 1:
                return URL_API1;
            case 2:
                return URL_API2;

            case 3:
                return URL_API3;
            default:
                throw new IOException("Dummy data error");
        }
    }

    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            pushError(e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public void detroy(Handler handler) {
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

}
