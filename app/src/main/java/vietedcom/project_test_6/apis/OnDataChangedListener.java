package vietedcom.project_test_6.apis;

/**
 * Created by Smile on 3/6/2016.
 */
public interface OnDataChangedListener<T> {
    void dataChanged(T t);
}
