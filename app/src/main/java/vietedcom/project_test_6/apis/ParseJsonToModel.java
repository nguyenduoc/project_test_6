package vietedcom.project_test_6.apis;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import vietedcom.project_test_6.Model;


/**
 * Created by Smile on 3/6/2016.
 */
public class ParseJsonToModel {
    public ArrayList<Model> parse(String json) throws JSONException {
        if (json == null || json.isEmpty()) return null;
        JSONArray jsonArray = new JSONArray(json);

        if (jsonArray == null || jsonArray.length() == 0) return null;

        ArrayList<Model> modelList = new ArrayList<>();
        for (int index = 0; index < jsonArray.length(); ++index) {

            JSONObject jsonObject = jsonArray.getJSONObject(index);
            if (jsonObject != null) {
                modelList.add(readJsonObject(jsonObject));
            }

        }
        return modelList;
    }

    private Model readJsonObject(JSONObject jsonObject) throws JSONException {
        if (jsonObject == null) return null;

        Model model = new Model();
        if (jsonObject.has(ApiKey.NAME))
            model.setName(jsonObject.getString(ApiKey.NAME));

        if (jsonObject.has(ApiKey.GENDER))
            model.setGender(jsonObject.getInt(ApiKey.GENDER));

        if (jsonObject.has(ApiKey.PROJECTS)) {
            JSONObject jsonObjectChild = jsonObject.getJSONObject(ApiKey.PROJECTS);
            if (jsonObjectChild != null) {
                Model.Project project = new Model.Project();
                if (jsonObjectChild.has(ApiKey.NAME))
                    project.setName(jsonObjectChild.getString(ApiKey.NAME));

                if (jsonObjectChild.has(ApiKey.ROLE)) {
                    project.setRole(jsonObjectChild.getString(ApiKey.ROLE));
                }

                model.setProject(project);

            }
        }

        return model;
    }
}
