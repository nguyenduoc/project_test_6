package vietedcom.project_test_6.apis;

/**
 * Created by Smile on 3/31/2016.
 */
public interface ApiKey {
    String NAME = "name";
    String GENDER = "gender";
    String PROJECTS = "projects";
    String ROLE = "role";
}
