package vietedcom.project_test_6;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Smile on 3/6/2016.
 */
public class Model implements Parcelable {

    public static final int MALE = 1;
    public static final int FEMALE = 0;

    private String name;
    private int gender;
    private Project project;

    public Model() {
    }

    protected Model(Parcel in) {
        name = in.readString();
        gender = in.readInt();
        project = in.readParcelable(Project.class.getClassLoader());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public static final Creator<Model> CREATOR = new Creator<Model>() {
        @Override
        public Model createFromParcel(Parcel in) {
            return new Model(in);
        }

        @Override
        public Model[] newArray(int size) {
            return new Model[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(gender);
        dest.writeParcelable(project, flags);
    }


    public static class Project implements Parcelable {
        private String name;
        private String role;

        public Project() {
        }

        protected Project(Parcel in) {
            name = in.readString();
            role = in.readString();
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public static final Creator<Project> CREATOR = new Creator<Project>() {
            @Override
            public Project createFromParcel(Parcel in) {
                return new Project(in);
            }

            @Override
            public Project[] newArray(int size) {
                return new Project[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(name);
            dest.writeString(role);
        }
    }
}
